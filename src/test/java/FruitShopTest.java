import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by user on 26/05/2016.
 */

public class FruitShopTest {


    @Test
    public void iteration1() {
        Supermarket superMarche = new Supermarket("Pommes:1", "Bananes:1.5", "Cerises:0.75");


        superMarche.shop("Pommes");
        assertThat(superMarche.ammount()).isEqualTo(100);

        superMarche.shop("Cerises");
        assertThat(superMarche.ammount()).isEqualTo(175);

        superMarche.shop("Cerises");
        assertThat(superMarche.ammount()).isEqualTo(250);
    }

    @Test
    public void iteration2DuplicationCerises() {
        int numberOfLots = 2;

        Supermarket superMarche = new Supermarket("Pommes:1", "Bananes:1.5", "Cerises:0.75")
                //fruit : reduction to apply
                .reductionFor("Cerises:0.20", numberOfLots);


        superMarche.shop("Cerises");
        assertThat(superMarche.ammount()).isEqualTo(75);

        superMarche.shop("Pommes");
        assertThat(superMarche.ammount()).isEqualTo(175);

        superMarche.shop("Cerises");
        assertThat(superMarche.ammount()).isEqualTo(230);

        superMarche.shop("Bananes");
        assertThat(superMarche.ammount()).isEqualTo(380);

        superMarche.shop("Cerises");
        assertThat(superMarche.ammount()).isEqualTo(455);

        superMarche.shop("Cerises");
        assertThat(superMarche.ammount()).isEqualTo(510);

        superMarche.shop("Pommes");
        assertThat(superMarche.ammount()).isEqualTo(610);
    }

    @Test
    public void iteration3() {
        Supermarket superMarche = new Supermarket("Pommes:1", "Bananes:1.5", "Cerises:0.75")
                .reductionFor("Cerises:0.30", 2)
                .reductionFor("Bananes:1.5", 2);

        superMarche.shop("Cerises");
        assertThat(superMarche.ammount()).isEqualTo(75);

        superMarche.shop("Cerises");
        assertThat(superMarche.ammount()).isEqualTo(120);


        superMarche.shop("Bananes");
        assertThat(superMarche.ammount()).isEqualTo(270);


        superMarche.shop("Bananes");
        assertThat(superMarche.ammount()).isEqualTo(270);

    }

    @Test
    public void iteration4() {
        Supermarket superMarche = new Supermarket("Pommes, Apples, Mele:1", "Bananes:1.5", "Cerises:0.75")
                .reductionFor("Cerises:0.20", 2)
                .reductionFor("Bananes:1.5", 2);

        superMarche.shop("Cerises");
        assertThat(superMarche.ammount()).isEqualTo(75);

        superMarche.shop("Apples");
        assertThat(superMarche.ammount()).isEqualTo(175);


        superMarche.shop("Cerises");
        assertThat(superMarche.ammount()).isEqualTo(230);


        superMarche.shop("Bananes");
        assertThat(superMarche.ammount()).isEqualTo(380);


        superMarche.shop("Bananes");
        assertThat(superMarche.ammount()).isEqualTo(380);

    }

    @Test
    public void iteration5() {
        Supermarket superMarche = new Supermarket("Pommes, Apples, Mele:1", "Bananes:1.5", "Cerises:0.75")
                .reductionFor("Cerises:0.20", 2)
                .reductionFor("Bananes:1.5", 2)
                .reductionFor("Mele:1.5", 2)
                .reductionFor("Apples:1", 3);

        superMarche.shop("Mele");
        assertThat(superMarche.ammount()).isEqualTo(100);

        superMarche.shop("Apples");
        assertThat(superMarche.ammount()).isEqualTo(200);


        superMarche.shop("Apples");
        assertThat(superMarche.ammount()).isEqualTo(300);


        superMarche.shop("Pommes");
        assertThat(superMarche.ammount()).isEqualTo(400);

        superMarche.shop("Apples");
        assertThat(superMarche.ammount()).isEqualTo(400);

        superMarche.shop("Mele");
        assertThat(superMarche.ammount()).isEqualTo(450);

        superMarche.shop("Cerises");
        assertThat(superMarche.ammount()).isEqualTo(525);


        superMarche.shop("Cerises");
        assertThat(superMarche.ammount()).isEqualTo(580);


    }

    @Test
    public void iteration5prime() {
        Supermarket superMarche = new Supermarket("Pommes, Apples, Mele:1", "Bananes:1.5", "Cerises:0.75")
                .reductionFor("Cerises:0.20", 2)
                .reductionFor("Bananes:1.5", 2)
                .reductionFor("Mele:1", 2)
                .reductionFor("Apples:1", 3);

        superMarche.shopCSV("Mele,Apples,Apples,Pommes,Apples,Mele,Cerises,Cerises,Bananes");
        assertThat(superMarche.ammount()).isEqualTo(680);


    }

    @Test
    public void iteration6() {
        Supermarket superMarche = new Supermarket("Pommes, Apples, Mele:1", "Bananes:1.5", "Cerises:0.75")
                .reductionFor("Cerises:0.20", 2)
                .reductionFor("Bananes:1.5", 2)
                .reductionFor("Mele:1", 2)
                .reductionFor("Apples:1", 3);

        superMarche.shop("Mele,Apples,Apples,Mele");
        assertThat(superMarche.ammount()).isEqualTo(200);

        superMarche.shop("Bananes");
        assertThat(superMarche.ammount()).isEqualTo(680);

        superMarche.shop("Mele,Apples,Apples,Pommes,Apples,Mele,Cerises,Cerises,Bananes");
        assertThat(superMarche.ammount()).isEqualTo(680);


    }
}
