package parsers;

import fruit.Fruit;
import fruit.reduction.Reduction;

import java.util.Map;

/**
 * Created by user on 26/05/2016.
 */
public interface ParserReduction {
    Reduction parse(String fruitReduction, Map<String, Fruit> fruits);
}
