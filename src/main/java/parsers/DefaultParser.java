package parsers;

import fruit.Fruit;
import fruit.FruitFactory;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by user on 26/05/2016.
 */
public class DefaultParser implements Parser {

    public static final String SEPARATOR = ":";
    public static final int NAME_INDEX = 0;
    public static final int PRICE_INDEX = 1;
    private static final String SEPARATOR_NAMES = ", ";

    public Map<String, Fruit> parse(String[] fruitsInput) {
        Map<String, Fruit> fruits = new HashMap<>();
        for (String currentFruit : fruitsInput) {
            String[] splitFruit = currentFruit.split(SEPARATOR);
            double price = Double.parseDouble(splitFruit[PRICE_INDEX]);
            List<String> names = parseNames(splitFruit[NAME_INDEX]);
            for (String currentName : names) {
            Fruit fruit = FruitFactory.create(currentName, price);
                fruits.put(currentName, fruit);
            }
        }
        return fruits;
    }

    private List<String> parseNames(String s) {
        String fruitName = s;
        String[] splitNamesOfTheSameFruit = fruitName.split(SEPARATOR_NAMES);
        List<String> names = Arrays.asList(splitNamesOfTheSameFruit);
        return names;
    }

}
