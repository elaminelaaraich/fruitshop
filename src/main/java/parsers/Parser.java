package parsers;

import fruit.Fruit;

import java.util.Map;

/**
 * Created by user on 26/05/2016.
 */
public interface Parser {
    Map<String, Fruit> parse(String[] fruitsInput);
}
