package parsers;

import fruit.Fruit;
import fruit.reduction.DefaultReduction;
import fruit.reduction.Reduction;

import java.util.Map;

/**
 * Created by user on 26/05/2016.
 */
public class DefaultReductionParser implements ParserReduction {

    private static final int NAME_INDEX = 0;
    public static final String SEPARATOR = ":";
    private static final int REDUCTION_INDEX = 1;

    @Override
    public Reduction parse(String fruitReduction, Map<String, Fruit> fruits) {
        String[] splitFruit = fruitReduction.split(SEPARATOR);
        String fruitName = splitFruit[NAME_INDEX];
        double reduction = Double.parseDouble(splitFruit[REDUCTION_INDEX]);
        return new DefaultReduction(fruits.get(fruitName),reduction);
    }
}
