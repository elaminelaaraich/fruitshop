import fruit.Fruit;
import fruit.reduction.Reduction;
import parsers.ParserReduction;
import parsers.DefaultReductionParser;
import fruit.reduction.ReductionsCalculation;
import fruit.reduction.ReductionsFruit;
import parsers.DefaultParser;
import parsers.Parser;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by user on 26/05/2016.
 */
public class Supermarket {
    public static final int INITIAL_VALUE_ZERO = 0;
    public static final String SEPARATOR = ",";

    private final Map<String, Fruit> fruits;
    private final Parser parser;
    private final ParserReduction reductionParser;
    private final List<Fruit> caisse;
    private final ReductionsCalculation reductionsCalculation;

    public Supermarket(String... fruitsInput) {
        parser = new DefaultParser();
        this.fruits = parser.parse(fruitsInput);
        caisse = new ArrayList<>();
        reductionParser = new DefaultReductionParser();
        reductionsCalculation = new ReductionsFruit();
    }

    public void shop(String fruit) {
        Fruit currentFruit = this.fruits.get(fruit);
        caisse.add(currentFruit);

    }

    public double ammount() {
        double amount = INITIAL_VALUE_ZERO;
        reductionsCalculation.startCalculation();
        for (Fruit currentFruit : caisse) {
            amount += currentFruit.price() - reductionsCalculation.calculateReduction(currentFruit);
        }
        return amount;
    }


    public Supermarket reductionFor(String fruitReduction, int numberOfLots) {
        Reduction reduction = reductionParser.parse(fruitReduction, fruits)
                .applyFor(numberOfLots);
        reductionsCalculation.addReduction(reduction);
        return this;
    }

    public void shopCSV(String fruits) {
        String[] splitFruits = fruits.split(SEPARATOR);
        for (String currentFruit : splitFruits) {
            shop(currentFruit);
        }
    }
}
