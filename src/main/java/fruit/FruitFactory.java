package fruit;

import java.util.Arrays;
import java.util.List;

/**
 * Created by user on 26/05/2016.
 */
public class FruitFactory {


    public static Fruit create(String name, double price) {
        return new Fruit(name, price);
    }
}
