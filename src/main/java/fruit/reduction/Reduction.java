package fruit.reduction;

import fruit.Fruit;

/**
 * Created by user on 26/05/2016.
 */
public interface Reduction {
    Reduction applyFor(int numberOfLots);

    double calculate();

    boolean isFor(Fruit fruit);

    void initialize();
}
