package fruit.reduction;

import fruit.Fruit;

/**
 * Created by user on 26/05/2016.
 */
public interface ReductionsCalculation {
    double calculateReduction(Fruit currentFruit);

    void addReduction(Reduction reduction);

    void startCalculation();
}
