package fruit.reduction;

import exceptions.NoReductionIsFound;
import fruit.Fruit;

import java.util.*;

/**
 * Created by user on 26/05/2016.
 */
public class ReductionsFruit implements ReductionsCalculation {

    private static final double NO_REDUCTION = 0;
    private List<Reduction> reductions;


    public ReductionsFruit() {
        this.reductions = new ArrayList<>();
    }

    public double calculateReduction(Fruit currentFruit) {
        try {
            Reduction reduction = identifyReductionToApplyTo(currentFruit);
            return reduction.calculate();
        } catch (Exception e) {
            return NO_REDUCTION;
        }
    }

    private Reduction identifyReductionToApplyTo(Fruit fruit) {
        for (Reduction currentReduction : reductions) {
            if (currentReduction.isFor(fruit)) {
                return currentReduction;
            }
        }
        throw new NoReductionIsFound();
    }


    @Override
    public void addReduction(Reduction reduction) {
        reductions.add(reduction);
    }

    @Override
    public void startCalculation() {
        for (Reduction currentReductionBehavior : reductions) {
            currentReductionBehavior.initialize();
        }
    }


}
