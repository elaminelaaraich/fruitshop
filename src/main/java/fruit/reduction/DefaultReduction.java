package fruit.reduction;

import fruit.Fruit;


/**
 * Created by user on 26/05/2016.
 */
public class DefaultReduction implements Reduction {
    private static final double CENT_PER_EUR = 100;
    public static final int NO_REDUCTION = 0;
    private Fruit fruit;
    private double reduction;
    private int numberOfLots;
    private int counter;

    public DefaultReduction(Fruit fruit, double reduction) {
        this.fruit = fruit;
        this.reduction = reduction;
    }

    @Override
    public Reduction applyFor(int numberOfLots) {
        this.numberOfLots = numberOfLots;
        return this;
    }

    @Override
    public double calculate() {
        counter--;
        if (counter == 0) {
            initialize();
            return reduction * CENT_PER_EUR;
        }
        return NO_REDUCTION;
    }

    @Override
    public boolean isFor(Fruit fruit) {
        return fruit.equals(this.fruit);
    }

    @Override
    public void initialize() {
        counter = numberOfLots;
    }
}