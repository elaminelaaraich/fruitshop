package fruit;

/**
 * Created by user on 26/05/2016.
 */
public class Fruit {
    public static final int CENT_PER_EUR = 100;
    private final String name;
    private final double price;

    public Fruit(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public double price() {
        return price * CENT_PER_EUR;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Fruit)) return false;

        Fruit fruit = (Fruit) o;

        if (Double.compare(fruit.price, price) != 0) return false;
        return name.equals(fruit.name);

    }

}
